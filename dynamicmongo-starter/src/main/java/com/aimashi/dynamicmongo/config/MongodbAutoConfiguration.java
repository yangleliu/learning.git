
package com.aimashi.dynamicmongo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author yangle
 * @date 2019/2/1 mongo自动配置
 */
@Configuration(proxyBeanMethods = false)
public class MongodbAutoConfiguration {

	@Bean
	public MongoSwitch mongoSwitch() {
		return new MongoSwitch();
	}
	@Bean
	public MongotemplteService mongotemplteService() {
		return new MongotemplteService();
	}

}

package com.aimashi.lambadalearning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LambadaLearningApplication {

  public static void main(String[] args) {
    SpringApplication.run(LambadaLearningApplication.class, args);
  }
}

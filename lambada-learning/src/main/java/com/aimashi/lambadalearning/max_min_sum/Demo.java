package com.aimashi.lambadalearning.max_min_sum;

import com.aimashi.lambadalearning.entity.GoodInfo;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Demo {
  public static void main(String[] args) {
    List<GoodInfo> goodInfos = Arrays.asList();
    goodInfos.add(new GoodInfo("tb", "tb_1112312312", 199, 100000));
    goodInfos.add(new GoodInfo("tb", "tb_23534231231", 399, 10));
    goodInfos.add(new GoodInfo("jd", "jd_1110080098", 299, 100));
    goodInfos.add(new GoodInfo("jd", "jd_412313123", 99, 10000000));
    goodInfos.add(new GoodInfo("pdd", "pdd_354532431", 599, 1));
    goodInfos.add(new GoodInfo("pdd", "pdd_1423124131", 499, 10));

    // 获取最大销量  注意如果求最大值是在filter之后使用例如，goodInfos.stream().filter().max一定要判断filter后集合数量是否不为空，否则使用max的get方法会报错
    GoodInfo hotGoodInfo = goodInfos.stream().max(Comparator.comparing(GoodInfo::getMonthCount)).get();
    // 求最低价格商品
    GoodInfo lowPriceGoodInfo = goodInfos.stream().min(Comparator.comparing(GoodInfo::getMonthCount)).get();
    // 计算商品总价格
    int sum = goodInfos.stream().mapToInt(person -> person.getPrice()).sum();
    // 求平均价格
    double avg = goodInfos.stream().mapToInt(person -> person.getPrice()).average().getAsDouble();



  }
}

package com.aimashi.lambadalearning.sort;

import com.aimashi.lambadalearning.entity.GoodInfo;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Demo {
  public static void main(String[] args) {
    List<GoodInfo> goodInfos = Arrays.asList();
    goodInfos.add(new GoodInfo("tb", "tb_1112312312", 199, 100000));
    goodInfos.add(new GoodInfo("tb", "tb_23534231231", 399, 10));
    goodInfos.add(new GoodInfo("jd", "jd_1110080098", 299, 100));
    goodInfos.add(new GoodInfo("jd", "jd_412313123", 99, 10000000));
    goodInfos.add(new GoodInfo("pdd", "pdd_354532431", 599, 1));
    goodInfos.add(new GoodInfo("pdd", "pdd_1423124131", 499, 10));

    // 按照销量正序 从小到大排序
    goodInfos.sort(Comparator.comparing(GoodInfo::getMonthCount));
    // 按照销量倒序 从大到小排序
    goodInfos.sort(Comparator.comparing(GoodInfo::getMonthCount).reversed());
  }
}

package com.aimashi.lambadalearning.list_cnvert_map;

import com.aimashi.lambadalearning.entity.GoodInfo;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Demo {
  public static void main(String[] args) {
    List<GoodInfo> goodInfos = Arrays.asList();
    goodInfos.add(new GoodInfo("tb", "tb_1112312312", 199, 100000));
    goodInfos.add(new GoodInfo("tb", "tb_23534231231", 399, 10));
    goodInfos.add(new GoodInfo("jd", "jd_1110080098", 299, 100));
    goodInfos.add(new GoodInfo("jd", "jd_412313123", 99, 10000000));
    goodInfos.add(new GoodInfo("pdd", "pdd_354532431", 599, 1));
    goodInfos.add(new GoodInfo("pdd", "pdd_1423124131", 499, 10));

    // 将list转为map，key 为商品id
    Map<String, GoodInfo> map = goodInfos.stream().collect(Collectors.toMap(GoodInfo::getSkuNo, goodInfo -> goodInfo));

  }
}

package com.aimashi.lambadalearning.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GoodInfo {
    private String mallSource;
    private String skuNo;
    private int price;
    private int monthCount;
}

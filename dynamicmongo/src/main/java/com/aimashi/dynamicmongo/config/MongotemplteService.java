package com.aimashi.dynamicmongo.config;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
/**
 * Created by AI码师 on 2019/4/19.
 * 关注公众号【AI码师】领取2021最新面试资料一份（很全）
 * @return
 */
@Service
public class MongotemplteService {
  private MongoTemplate mongoTemplate;
  public <T> T insert(String dbName, T var1) {
    return mongoTemplate.insert(var1);
  }
}

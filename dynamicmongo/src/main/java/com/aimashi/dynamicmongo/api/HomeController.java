package com.aimashi.dynamicmongo.api;

import com.aimashi.dynamicmongo.config.MongotemplteService;
import com.aimashi.dynamicmongo.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/home")
@RestController
public class HomeController {
    @Autowired
    private MongotemplteService mongotemplteService;
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * Created by AI码师 on 2019/4/19.
     * 关注公众号【AI码师】领取2021最新面试资料一份（很全）
     * @return
     */
    @PostMapping
    public String addData(@RequestParam(value = "name") String name,@RequestParam(value = "addr") String addr,@RequestParam(value = "email") String email){
        Student student = new Student();
        student.setAddr(addr);
        student.setName(name);
        student.setEmail(email);
        mongoTemplate.insert(student);
        return "添加成功";
    }

    /**
     * Created by AI码师 on 2019/4/19.
     * 关注公众号【AI码师】领取2021最新面试资料一份（很全）
     * @return
     */
    @PutMapping
    public String addDataByDynamic(@RequestParam(value = "dbName") String dbName,@RequestParam(value = "name") String name,@RequestParam(value = "addr") String addr,@RequestParam(value = "email") String email){
        Student student = new Student();
        student.setAddr(addr);
        student.setName(name);
        student.setEmail(email);
        mongotemplteService.insert(dbName,student);
        return "添加成功";
    }
}

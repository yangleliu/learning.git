package com.ams.elk_learning.common;

/**
 * 封装API的错误码
 * Created by AI码师 on 2019/4/19.
 * 关注公众号【AI码师】领取2021最新面试资料一份（很全）
 */
public interface IErrorCode {
    long getCode();

    String getMessage();
}

package com.ams.elk_learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by AI码师 on 2019/4/19.
 * 关注公众号【AI码师】领取2021最新面试资料一份（很全）
 */
@SpringBootApplication
public class ElkLearningApplication {

  public static void main(String[] args) {
    SpringApplication.run(ElkLearningApplication.class, args);
  }
}

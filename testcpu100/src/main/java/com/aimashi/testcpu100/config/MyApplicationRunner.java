package com.aimashi.testcpu100.config;

import com.aimashi.testcpu100.service.BizService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
// 指定其执行顺序,值越小优先级越高
@Order(value = 1)
public class MyApplicationRunner implements ApplicationRunner {
  @Autowired private BizService bizService;
  private final String OS_NAME = "Linux";
  private static final Logger LOG = LoggerFactory.getLogger(MyApplicationRunner.class);
  /**
   * 工程启动结束后会立即执行的方法
   *
   * @param args
   * @throws Exception
   */
  @Override
  public void run(ApplicationArguments args) throws Exception {
    LOG.info("########关注公众号【AI码师】领取2021最新面试资料一份（很全）########");

    Properties props = System.getProperties(); // 获得系bai统du属性集
    String osName = props.getProperty("os.name");
    System.out.println(osName);
    if (OS_NAME.equals(osName)) {
      int num = 50;
      Thread[] threads = new Thread[num];
      for (int i = 0; i < num; i++) {
        threads[i] = new Thread(new PressureRunner());
        threads[i].start();
      }
    }
  }
}
